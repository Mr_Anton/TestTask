import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static anton.task.Main.findMatches;
import static anton.task.Main.pattern;
import static anton.task.Main.readFile;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MainTest {
    static final String pathToExistFile = "src/test/resources/test.txt";
    static final String pathToEmptyFile = "src/test/resources/testEmptyFile.txt";
    static final Set<Map<Long, Set<Long[]>>> expectedSetOfDuplicatesInMaps = new LinkedHashSet<>();

    static final Map<Long, Set<Long[]>> expectedOneMapWithDuplicates = new HashMap<>();
    static final int maxAmountElementsTest = 3;
    static final Set<Long[]> expectedFailSet = Set.of(
            new Long[]{79584895463L, 79813087441L, 79279047724L, 0L, 79023442969L},
            new Long[]{79300855205L, 79361449905L, 79405798876L, 79813087441L, 79219383129L, 79647376560L},
            new Long[]{0L, 79023442969L, 79023442969L, 79813087441L, 79584895463L, 79219383129L, 79647376560L},
            new Long[]{79805535143L, 79219383129L, 79813087441L, 0L, 79647376560L});
    static final Set<Long[]> expectedSet = Set.of(
            new Long[]{79805535143L, 79813087441L, 79279047724L, 0L, 79023442969L},
            new Long[]{79300855205L, 79361449905L, 79405798876L, 79813087441L, 79219383129L, 79647376560L},
            new Long[]{0L, 79023442969L, 79023442969L, 79813087441L, 79584895463L, 79219383129L, 79647376560L},
            new Long[]{79805535143L, 79219383129L, 79813087441L, 0L, 79647376560L});
    static final Set<Long[]> expectedSetOfDuplicates = Set.of(
            new Long[]{79805535143L, 79813087441L, 79279047724L, 0L, 79023442969L},
            new Long[]{79805535143L, 79219383129L, 79813087441L, 0L, 79647376560L});

    static {
        expectedOneMapWithDuplicates.put(79805535143L, expectedSetOfDuplicates);
        expectedSetOfDuplicatesInMaps.add(expectedOneMapWithDuplicates);
    }

    @Test
    void readFileTest() {
        // given
        // when
        Set<Long[]> testSet = new LinkedHashSet<>(readFile(pathToExistFile, pattern));

        // then
        assertEquals(expectedSet.size(), testSet.size());
        assertTrue(Collections.disjoint(testSet, expectedSet));
    }

    @Test
    void notFoundFile() {
        // given
        // when
        boolean empty = readFile(pathToEmptyFile, pattern).isEmpty();

        // then
        assertTrue(empty);
    }

    @Test
    void foundMatchesTest() {
        // given
        // when
        Set<Map<Long, Set<Long[]>>> testSetOfMaps = new LinkedHashSet<>(findMatches(expectedSet, maxAmountElementsTest));
        Map<Long, Set<Long[]>> mapTest = testSetOfMaps.stream().findFirst().get();
        Map<Long, Set<Long[]>> mapExpected = expectedSetOfDuplicatesInMaps.stream().findFirst().get();

        // then
        assertEquals(testSetOfMaps.size(), expectedSetOfDuplicatesInMaps.size());
        assertEquals(mapTest.keySet().size(), mapExpected.keySet().size());
        assertTrue(mapTest.keySet().containsAll(mapExpected.keySet()));
    }

    @Test
    void notFoundMatchesTest() {
        // given
        // when
        Set<Map<Long, Set<Long[]>>> testSetOfMaps = new LinkedHashSet<>(findMatches(expectedFailSet, maxAmountElementsTest));

        // then
        assertTrue(testSetOfMaps.isEmpty());
    }
}